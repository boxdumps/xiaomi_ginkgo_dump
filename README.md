## ginkgo-user 11 RKQ1.201004.002 V12.5.1.0.RCOINXM release-keys
- Manufacturer: xiaomi
- Platform: trinket
- Codename: ginkgo
- Brand: xiaomi
- Flavor: ginkgo-user
- Release Version: 11
- Id: RKQ1.201004.002
- Incremental: V12.5.1.0.RCOINXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: xiaomi/ginkgo/ginkgo:11/RKQ1.201004.002/V12.5.1.0.RCOINXM:user/release-keys
- OTA version: 
- Branch: ginkgo-user-11-RKQ1.201004.002-V12.5.1.0.RCOINXM-release-keys-random-text-27112599815505
- Repo: xiaomi_ginkgo_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
